import time
import h5py as h5
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#path_setup_json = "datasheets/setup_smartphone.json"
path_setup_json = "datasheets/setup_laptop.json" # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# -------------------------------------------------------------------------------------------#2-start
# TODO: Initialize the data structure
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#UUID des Sensors für spätere Versuchsdurchführung speichern
uuid_sensor = "1ee847be-fddd-6ee4-892a-68c4555b0981"

#Daten des Sensors in ein Schema sortieren
sensor_data = {
    uuid_sensor: {
        'acceleration_x': [],
        'acceleration_y': [],
        'acceleration_z': [],
        'timestamp': []
        }
    }

# ---------------------------------------------------------------------------------------------#2-end


# -------------------------------------------------------------------------------------------#3-start
# TODO: Measure the probe
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
i2c = board.I2C()
accelerometer = adafruit_adxl34x.ADXL345(i2c)

#Ausgabe der Messzeit, um zu schauen, wie lange dieser noch braucht
starttime = time.time()
print(starttime)

#Solange der Messzeitpunkt innerhalb der 20 Sekunden der Messung liegt, sollen die Beschleunigungen gemessen werden
while time.time() - starttime < measure_duration_in_s:
    acceleration = accelerometer.acceleration
    timestamp = time.time() - starttime
    print(timestamp)
    
    #Hinzufügen des jeweiligen Beschleunigungs- und Zeitwert in die vorher definierte Datenstruktur
    sensor_data[uuid_sensor]['acceleration_x'].append(acceleration[0])
    sensor_data[uuid_sensor]['acceleration_y'].append(acceleration[1])
    sensor_data[uuid_sensor]['acceleration_z'].append(acceleration[2])
    sensor_data[uuid_sensor]['timestamp'].append(timestamp)

    time.sleep(0.001) #1 ms Pause innerhalb des jeweiligen Schleifendurchlaufs
    
# ---------------------------------------------------------------------------------------------#3-end

# ---------------------------------------------------------------------------------------------#4-start
# TODO: Write results in hdf5
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#Erstellen der hdf5-Datei, in denen die Messdaten aufgenommen werden
filename = path_h5_file
with h5.File(filename, "w") as file:
    #Initialisierung der geforderten Datenstruktur laut Aufgabenstellung
    group = file.create_group("RawData")
    
    subgroup = group.create_group(uuid_sensor)
    #Definition der benötigten Einheiten für die aufgenommenen Werte
    units = ['m/s^2', 'm/s^2','m/s^2', 's']
    
    i = 0
    #Hinzufügen der jeweiligen Beschleunigung und Zeit mit der vorher definierten Einheit
    for key in sensor_data[uuid_sensor].keys():
        dataset = subgroup.create_dataset(key, data = sensor_data[uuid_sensor][key])
        dataset.attrs['unit'] = units[i]
        i = i+1
#Schließen der hdf5-Datei      
file.close()
# ---------------------------------------------------------------------------------------------#4-end

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
